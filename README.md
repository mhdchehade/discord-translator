# Discord Translator


**invite:** https://discord.com/oauth2/authorize?client_id=734995156264157294&scope=bot&permissions=8


**dependencies:**

translate-shell:


```
sudo pacman -S translate-shell
```

<br />
Discord.py:

https://pythondiscord.com/pages/resources/guides/discordpy/

```
python3 -m pip install -U discord.py
```

<br />

**running:**

For no automatic rebooting after reboot command:

```
nohup python translate.py &
```



fish automatic reboot command: (not recommended)

```
nohup fish ./run.sh &
```

**Token:**

It is important to put the token in the same directory of translate.py


```
echo "ThisISYourTokenFromDiscorD.69-wfe.sdDGDGucHI_uwu-O90-ssMmp" > token
```
