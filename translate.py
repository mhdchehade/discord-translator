# invite link https://discord.com/oauth2/authorize?client_id=734995156264157294&scope=bot&permissions=8

import discord
from discord.ext import commands
import sys
#from googletrans import Translator
from Translator import Translator
import re
import os
import json



TOKEN = ""
DATA_DIR = '_data'
translator = Translator()
users_list={"users_list":[]}
command_given = False
announced_channel = ''
LOG = 735537445918998679 #logging channel
DESC = "Hi, I am google translate chan, you can use the commands below. ILY all!"
PRE = ['!t', '！T', '！t', '！ｔ', 't']
GITLAB = 'Discord translator code on gitlab: https://gitlab.com/mhdchehade/discord-translator'
INVITE = 'Discord translator invite link: ` https://discord.com/oauth2/authorize?client_id=734995156264157294&scope=bot&permissions=8`'

# removed 'hi'
LANGUAGES = ['af', 'sq', 'am', 'ar', 'hy', 'az', 'eu', 'be', 'bn', 'bs', 'bg', 'ca', 'ceb', 'ny', 'zh-cn', 'zh-tw', 'co', 'hr', 'cs', 'da', 'nl', 'en', 'eo', 'et', 'tl', 'fi', 'fr', 'fy', 'gl', 'ka', 'de', 'el', 'gu', 'ht', 'ha', 'haw', 'iw', 'hmn', 'hu', 'is', 'ig', 'id', 'ga', 'it', 'ja', 'jw', 'kn', 'kk', 'km', 'ko', 'ku', 'ky', 'lo', 'la', 'lv', 'lt', 'lb', 'mk', 'mg', 'ms', 'ml', 'mt', 'mi', 'mr', 'mn', 'my', 'ne', 'no', 'ps', 'fa', 'pl', 'pt', 'pa', 'ro', 'ru', 'sm', 'gd', 'sr', 'st', 'sn', 'sd', 'si', 'sk', 'sl', 'so', 'es', 'su', 'sw', 'sv', 'tg', 'ta', 'te', 'th', 'tr', 'uk', 'ur', 'uz', 'vi', 'cy', 'xh', 'yi', 'yo', 'zu', 'fil', 'he']

bot = commands.Bot(command_prefix=PRE, description = DESC, activity=discord.Game(name='Google Translate'))


# open data
if not os.path.isfile(DATA_DIR):
    file = open(DATA_DIR + '/' + "user_data.json", "r")
    x = file.read()
    users_list = json.loads(x)
    print("loaded data:", users_list)

def save_data():
    if not os.path.exists(DATA_DIR):
        os.makedirs(DATA_DIR)

    y = json.dumps(users_list)
    tf = open(DATA_DIR + '/' + 'user_data.json', 'w')
    tf.write(y)
    tf.close()

async def log(*args):
    #print('sending log') https://unix.stackexchange.com/questions/3886/difference-between-nohup-disown-and#148698
    msg = 'LOG: '
    for arg in args:
        msg += str(arg) + ' '
    await bot.get_channel(LOG).send(msg)

def nick_name(auth):
    if isinstance(auth, discord.User):
        return auth.name
    if isinstance(auth.nick, str):
        return auth.nick
    return auth.name

@bot.command()
async def atus(ctx): # change the status of the bot
    """Change the status of the bot (!tatus playing Google Translate or !tatus streaming My song *url*)"""
    global command_given
    command_given = True

    await log("status change by", ctx.message.author, "content", ctx.message.content)

    msg = ctx.message.content.split()
    msg.pop(0)

    if len(msg) == 0:
        return

    playing = ['playing', 'play', 'g', 'game']
    steaming = ['streaming', 's', 'stream']
    listening = ['listening', 'l', 'listen']
    watching = ['watching', 'w', 'watch']
    # Setting `Playing ` status
    if msg[0] in playing:
        msg.pop(0)
        await bot.change_presence(activity=discord.Game(name=' '.join(msg)))

    # Setting `Streaming ` status
    elif msg[0] in steaming:
        msg.pop(0)
        await bot.change_presence(activity=discord.Streaming(name=' '.join(msg[:-1]), url=msg[-1]))

    # Setting `Listening ` status
    elif msg[0] in listening:
        msg.pop(0)
        await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name=' '.join(msg)))

    # Setting `Watching ` status
    elif msg[0] in watching:
        msg.pop(0)
        await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=' '.join(msg)))

    else: # defaulting to playing
        await bot.change_presence(activity=discord.Game(name=' '.join(msg)))

@bot.command()
async def addme(ctx):
    """Translate all the messages sent on this channel and send them to you as DM. (!taddme jp cn tr ... en) source languages and the last one is the destination"""
    global command_given
    global users_list
    command_given = True

    msg = ctx.message.content.split()
    msg.pop(0)
    if len(msg) == 0:
        msg = ['zh-CN', 'zh-TW', 'ja', 'en']
    msg_lang = msg[:-1]
    des_lang = msg[-1]

    for counter, value in enumerate(msg_lang):
        if value == 'jp':
            msg_lang[counter] = 'ja'
        if value == 'cn':
            msg_lang[counter] = 'zh-CN'

    # no to add people twice
    for user in users_list["users_list"]:
        if user[0] == ctx.author.id and user[1] == ctx.channel.id:
            return

    users_list["users_list"].append([ctx.author.id, ctx.channel.id, msg_lang, des_lang])
    await log("added: ", ctx.author.name, ", on: ", str(ctx.channel.name), users_list["users_list"][-1][2])
    if not '<' in ctx.message.content:
        await ctx.message.delete()
    save_data()

@bot.command()
async def eboot(ctx):
    global command_given
    command_given = True
    await log("exiting! command by",ctx.author, 'reason', ctx.message.content)
    sys.exit(0)
    #os.excel("restart.sh","")

@bot.command()
async def r(ctx):
    """"Translate the message to requested language. (!tr cn Hi, how are you)"""
    await translate(ctx, False, False)

@bot.command()
async def R(ctx):
    """"Translate the message to requested language. (!tr cn Hi, how are you)"""
    await translate(ctx, False, False)

@bot.command()
async def p(ctx):
    """"Translate the message to requested language and display the pronunciation. (!tr cn Hi, how are you)"""
    await translate(ctx, False, True)

@bot.command()
async def d(ctx):
    """"Translate the message to requested language and delete the original message. (!td ja Hi, how are you)"""
    await translate(ctx, True, False)

@bot.command()
async def invite(ctx):
    """sends the invite and gitlab  link"""
    global command_given
    command_given = True

    await ctx.channel.send(GITLAB)
    await ctx.channel.send(INVITE)

@bot.command()
async def roll(ctx):
    """hmm"""
    global command_given
    command_given = True

    mentions = ctx.message.mentions

    if len(mentions) == 0 or len(mentions) > 2 or ('<@here>' in mentions) or ('<@everyone>' in mentions):
        await ctx.channel.send('no')
        #await ctx.message.delete()
        return

    msg = ctx.message.content.split()
    msg.pop(0)
    msg = ' '.join(msg)

    roles = ctx.guild.roles
    for men in mentions:
        if men in roles:
            return
        await ctx.channel.send(nick_name(men) + ': ' + translator.translate(re.sub('<[^>]+>', '', msg), dest='en').text)
    await ctx.message.delete()

async def translate(ctx, delete_original=False, display_pronunciation = False):
    global command_given
    command_given = True

    lang = 'en'
    skip = False

    msg = ctx.message.content.split()
    msg.pop(0)

    if len(msg) == 0:
        return

    if msg[0] == 'cn':
        msg[0] = 'zh-CN'
    if msg[0] == 'tw':
        msg[0] = 'zh-TW'
    if msg[0] == 'jp':
        msg[0] = 'ja'
    if msg[0].lower() in LANGUAGES:
        lang = msg[0] #.lower() (trans bug)
        msg.pop(0)
    msg_auth = nick_name(ctx.message.author) + ': '

    msg = ' '.join(msg)

    msg = re.sub('<[^>]+>', '', msg)
    translation = translator.translate(msg, dest=lang)
    msg = translation.text
    if display_pronunciation and not translation.pronunciation is None:
        msg = msg + '\n' + translation.pronunciation
    await ctx.channel.send(msg_auth + msg)
    if delete_original:
        await ctx.message.delete()

@bot.event
async def on_message(message):
    await bot.process_commands(message)
    global command_given

    if command_given:
        command_given = False
        return

    if message.guild == None:
        return

    if message.author.bot or ('http' in message.content) or ('\ ' in message.content) or ('tr ' in message.content):
        #print(f"ignored message: {message.content}")
        #await log("message discarded: bot or http")
        return


    #if len(message.attachments) > 0:
        #await log("message by", message.author.name,"discarded: have attachments")
    #    return

    for user in users_list["users_list"]:

        if (message.author.id == user[0]) or (message.channel.id != user[1]):
            #await log("message discarded: sent by user")
            continue

        t_msg = translator.translate(message.content, dest=user[3])

        if not (t_msg.src in user[2]):
            # await log("message discarded: msg src ", t_msg.src, ", not in ", user[2])
            #print(f'msg src: {t_msg.src}, user[2]: {user[2]}')
            continue

        v = ''
        global announced_channel
        new_announced_channel = 'in channel: ' + message.channel.name + ', in server ' + message.guild.name
        if announced_channel != new_announced_channel:
            announced_channel = new_announced_channel
            v = announced_channel + '\n'
            #await user[0].send(new_announced_channel)
        mmsg = v
        mmsg += str(message.author.name)
        mmsg +=  ': '
        mmsg += message.content
        mmsg += '\n'
        if not t_msg.phonetics is None:
            mmsg += t_msg.phonetics + '\n'
        mmsg += str(message.author.name) + ': ' + str(t_msg.text)
        # print('sending',mmsg)
        m_user = await bot.fetch_user(user[0])
        await m_user.send(mmsg)


f = open("token", "r")
TOKEN = f.read()
TOKEN = TOKEN[:-1]

bot.run(TOKEN)
