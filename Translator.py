import subprocess
import json

class Translation:
    text = None
    pronunciation = None
    src = None
    phonetics = None
    original = None

class Translator:
    translation = Translation()

    def translate(self, msg, dest='en'):
        options = '-dump'
        command = 'trans ' + ' :' + dest + ' "' + msg + '" ' + options
        res = subprocess.check_output(command, shell = True)
        res = res.decode('utf-8').splitlines()

        m_res = ''
        for line in res:
            if not (('[' in line) or (']' in line)):
                continue
            m_res += line

        m_res = m_res.replace("\n", "")
        m_res = json.loads(m_res)

        self.translation.original = msg
        self.translation.text = m_res[0][0][0]
        self.translation.src = m_res[2]

        self.translation.pronunciation = None
        self.translation.phonetics = None
        if len(m_res[0]) > 1:
            if len(m_res[0][1]) > 2:
                self.translation.pronunciation = m_res[0][1][2]
                if len(m_res[0][1]) > 3:
                    self.translation.phonetics = m_res[0][1][3]

        print(f'original: {self.translation.original}')
        print(f'text: {self.translation.text}')
        print(f'pronunciation: {self.translation.pronunciation}')
        print(f'src: {self.translation.src}')
        print(f'phonetics: {self.translation.phonetics}')
        return self.translation
